const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// create student schema & model
const ScoreSchema = new Schema({
    user: {
        type: String,
    },
    totalScore: {
        type: Number,
        required: [true, 'totalScore field is required']
    },
    frames: {
        type: Array
    }
});


const Score = mongoose.model('score',ScoreSchema);

module.exports = Score;
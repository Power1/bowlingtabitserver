const express = require('express');
const router = express.Router();
const Score = require('../models/score');

router.get('/scores',function(req,res,next) {
    Score.find({}).sort({totalScore: -1}).limit(5).then(function(scores){
        res.send(scores);
    }).catch(next);
});

router.post('/saveScore',function(req,res,next){
    Score.create(req.body).then(function(score){
        res.send(score);
    }).catch(next);
});

module.exports = router;